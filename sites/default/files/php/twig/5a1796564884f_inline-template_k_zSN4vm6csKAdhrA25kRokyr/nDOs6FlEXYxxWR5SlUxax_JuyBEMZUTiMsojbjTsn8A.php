<?php

/* {# inline_template_start #}geolocation-demo/proximity_filter_and_sort?proximity_lat={{ field_geolocation_demo_single__lat }}&proximity_lng={{ field_geolocation_demo_single__lng }} */
class __TwigTemplate_82a72ba1b6b60d3652f032ef93f23ac9937a032b08e3223dbe34f0165fbe6f9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "geolocation-demo/proximity_filter_and_sort?proximity_lat=";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["field_geolocation_demo_single__lat"]) ? $context["field_geolocation_demo_single__lat"] : null), "html", null, true));
        echo "&proximity_lng=";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["field_geolocation_demo_single__lng"]) ? $context["field_geolocation_demo_single__lng"] : null), "html", null, true));
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}geolocation-demo/proximity_filter_and_sort?proximity_lat={{ field_geolocation_demo_single__lat }}&proximity_lng={{ field_geolocation_demo_single__lng }}";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}geolocation-demo/proximity_filter_and_sort?proximity_lat={{ field_geolocation_demo_single__lat }}&proximity_lng={{ field_geolocation_demo_single__lng }}", "");
    }
}
